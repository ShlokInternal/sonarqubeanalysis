/*************************************************************
** Class generated by CodeTrigger, Version 6.1.0.7
** This class was generated on 09-08-2018 PM 02:30:18
** Changes to this file may cause incorrect behaviour and will be lost if the code is regenerated
**************************************************************/
using System;
using System.Collections.Generic;
using CGSampleMVCWebAPI.Data;
using CGSampleMVCWebAPI.Data.Interfaces;
using CGSampleMVCWebAPI.Business.Interfaces;
using CGSampleMVCWebAPI.Business.Repository.Interfaces;

namespace CGSampleMVCWebAPI.Business
{
	///<Summary>
	///Class definition
	///This is the definition of the class BOIdentityUserClaim.
	///</Summary>
	public partial class BOIdentityUserClaim : zCGSampleMVCWebAPIConn_BaseBusiness, IBOIdentityUserClaim, IQueryableCollection, IUnitOfWorkEntity
	{
		#region member variables
		protected Int32? _id;
		protected Int32? _userId;
		protected string _claimType;
		protected string _claimValue;
		protected bool _isDirty = false;
		/*collection member objects*******************/
		/*********************************************/
		/*repositories*********************************/
		protected IIdentityUserClaimRepository _iIdentityUserClaimRepository;
		/*********************************************/
		#endregion

		#region class methods
		///<Summary>
		///Constructor
		///This is the default constructor
		///</Summary>
		///<returns>
		///void
		///</returns>
		///<parameters>
		///
		///</parameters>
		public BOIdentityUserClaim()
		{
		}

		///<Summary>
		///Initializer
		///Initializer using primary key(s)
		///</Summary>
		///<returns>
		///void
		///</returns>
		///<parameters>
		///Int32 id
		///</parameters>
		public void Init(Int32 id)
		{
			try
			{
				IDAOIdentityUserClaim daoIdentityUserClaim = _iIdentityUserClaimRepository.SelectOne(id);
				_id = daoIdentityUserClaim.Id;
				_userId = daoIdentityUserClaim.UserId;
				_claimType = daoIdentityUserClaim.ClaimType;
				_claimValue = daoIdentityUserClaim.ClaimValue;
			}
			catch
			{
				throw;
			}
		}

		///<Summary>
		///Constructor
		///This constructor initializes the business object from its respective data object
		///</Summary>
		///<returns>
		///void
		///</returns>
		///<parameters>
		///DAOIdentityUserClaim
		///</parameters>
		protected internal BOIdentityUserClaim(IDAOIdentityUserClaim daoIdentityUserClaim)
		{
			try
			{
				_id = daoIdentityUserClaim.Id;
				_userId = daoIdentityUserClaim.UserId;
				_claimType = daoIdentityUserClaim.ClaimType;
				_claimValue = daoIdentityUserClaim.ClaimValue;
			}
			catch
			{
				throw;
			}
		}

		///<Summary>
		///SaveNew
		///This method persists a new IdentityUserClaim record to the store
		///</Summary>
		///<returns>
		///void
		///</returns>
		///<parameters>
		///
		///</parameters>
		public virtual void SaveNew()
		{
			IDAOIdentityUserClaim daoIdentityUserClaim = _iIdentityUserClaimRepository.New();
			RegisterDataObject( _iIdentityUserClaimRepository, _iIdentityUserClaimRepository.BaseData(daoIdentityUserClaim));
			BeginTransaction( _iIdentityUserClaimRepository, "savenewBOIdentityUserClaim");
			try
			{
				daoIdentityUserClaim.UserId = _userId;
				daoIdentityUserClaim.ClaimType = _claimType;
				daoIdentityUserClaim.ClaimValue = _claimValue;
				_iIdentityUserClaimRepository.Insert(daoIdentityUserClaim);
				CommitTransaction( _iIdentityUserClaimRepository);
				
				_id = daoIdentityUserClaim.Id;
				_userId = daoIdentityUserClaim.UserId;
				_claimType = daoIdentityUserClaim.ClaimType;
				_claimValue = daoIdentityUserClaim.ClaimValue;
				_isDirty = false;
			}
			catch
			{
				RollbackTransaction(_iIdentityUserClaimRepository, "savenewBOIdentityUserClaim");
				throw;
			}
		}
		///<Summary>
		///Update
		///This method updates one IdentityUserClaim record in the store
		///</Summary>
		///<returns>
		///void
		///</returns>
		///<parameters>
		///BOIdentityUserClaim
		///</parameters>
		public virtual void Update()
		{
			IDAOIdentityUserClaim daoIdentityUserClaim = _iIdentityUserClaimRepository.New();
			RegisterDataObject(_iIdentityUserClaimRepository, _iIdentityUserClaimRepository.BaseData(daoIdentityUserClaim));
			BeginTransaction(_iIdentityUserClaimRepository, "updateBOIdentityUserClaim");
			try
			{
				daoIdentityUserClaim.Id = _id;
				daoIdentityUserClaim.UserId = _userId;
				daoIdentityUserClaim.ClaimType = _claimType;
				daoIdentityUserClaim.ClaimValue = _claimValue;
				_iIdentityUserClaimRepository.Update(daoIdentityUserClaim);
				CommitTransaction(_iIdentityUserClaimRepository);
				
				_id = daoIdentityUserClaim.Id;
				_userId = daoIdentityUserClaim.UserId;
				_claimType = daoIdentityUserClaim.ClaimType;
				_claimValue = daoIdentityUserClaim.ClaimValue;
				_isDirty = false;
			}
			catch
			{
				RollbackTransaction(_iIdentityUserClaimRepository, "updateBOIdentityUserClaim");
				throw;
			}
		}
		///<Summary>
		///Delete
		///This method deletes one IdentityUserClaim record from the store
		///</Summary>
		///<returns>
		///void
		///</returns>
		///<parameters>
		///
		///</parameters>
		public virtual void Delete()
		{
			IDAOIdentityUserClaim daoIdentityUserClaim = _iIdentityUserClaimRepository.New();
			RegisterDataObject(_iIdentityUserClaimRepository, _iIdentityUserClaimRepository.BaseData(daoIdentityUserClaim));
			BeginTransaction(_iIdentityUserClaimRepository, "deleteBOIdentityUserClaim");
			try
			{
				daoIdentityUserClaim.Id = _id;
				_iIdentityUserClaimRepository.Delete(daoIdentityUserClaim);
				CommitTransaction(_iIdentityUserClaimRepository);
			}
			catch
			{
				RollbackTransaction(_iIdentityUserClaimRepository, "deleteBOIdentityUserClaim");
				throw;
			}
		}
		
		///<Summary>
		///IdentityUserClaimCollection
		///This method returns the collection of BOIdentityUserClaim objects
		///</Summary>
		///<returns>
		///IList[IBOIdentityUserClaim]
		///</returns>
		///<parameters>
		///
		///</parameters>
		public static IList<IBOIdentityUserClaim> IdentityUserClaimCollection(IIdentityUserClaimRepository iIdentityUserClaimRepository)
		{
			try
			{
				IList<IBOIdentityUserClaim> boIdentityUserClaimCollection = new List<IBOIdentityUserClaim>();
				IList<IDAOIdentityUserClaim> daoIdentityUserClaimCollection = iIdentityUserClaimRepository.SelectAll();
				
				foreach(IDAOIdentityUserClaim daoIdentityUserClaim in daoIdentityUserClaimCollection)
					boIdentityUserClaimCollection.Add(new BOIdentityUserClaim(daoIdentityUserClaim));
				
				return boIdentityUserClaimCollection;
			}
			catch
			{
				throw;
			}
		}
		
		
		///<Summary>
		///IdentityUserClaimCollectionCount
		///This method returns the collection count of BOIdentityUserClaim objects
		///</Summary>
		///<returns>
		///Int32
		///</returns>
		///<parameters>
		///
		///</parameters>
		public static Int32 IdentityUserClaimCollectionCount(IIdentityUserClaimRepository iIdentityUserClaimRepository)
		{
			try
			{
				Int32 objCount = iIdentityUserClaimRepository.SelectAllCount();
				return objCount;
			}
			catch
			{
				throw;
			}
		}
		
		
		///<Summary>
		///IdentityUserClaimCollectionFromCriteria
		///This method returns the collection of projections, ordered and filtered by optional criteria
		///</Summary>
		///<returns>
		///IList<IBOIdentityUserClaim>
		///</returns>
		///<parameters>
		///ICriteria icriteria
		///</parameters>
		public virtual IDictionary<string, IList<object>> Projections(object o)
		{
			try
			{
				ICriteria icriteria = (ICriteria)o;
				IList<IDataProjection> lstDataProjection = (icriteria == null) ? null : icriteria.ListDataProjection();
				IList<IDataCriterion> lstDataCriteria = (icriteria == null) ? null : icriteria.ListDataCriteria();
				IList<IDataOrderBy> lstDataOrder = (icriteria == null) ? null : icriteria.ListDataOrder();
				IDataSkip dataSkip = (icriteria == null) ? null : icriteria.DataSkip();
				IDataTake dataTake = (icriteria == null) ? null : icriteria.DataTake();
				return _iIdentityUserClaimRepository.SelectAllByCriteriaProjection(lstDataProjection, lstDataCriteria, lstDataOrder, dataSkip, dataTake);
			}
			catch
			{
				throw;
			}
		}
		
		///<Summary>
		///Collection<T>
		///This method returns the collection of T objects, ordered and filtered by optional criteria
		///</Summary>
		///<returns>
		///IList<T>
		///</returns>
		///<parameters>
		///object o (ICriteria)
		///</parameters>
		public virtual IList<T> Collection<T>(object o)
		{
			try
			{
				ICriteria icriteria = (ICriteria)o;
				IList<T> boIdentityUserClaimCollection = new List<T>();
				IList<IDataCriterion> lstDataCriteria = (icriteria == null) ? null : icriteria.ListDataCriteria();
				IList<IDataOrderBy> lstDataOrder = (icriteria == null) ? null : icriteria.ListDataOrder();
				IDataSkip dataSkip = (icriteria == null) ? null : icriteria.DataSkip();
				IDataTake dataTake = (icriteria == null) ? null : icriteria.DataTake();
				IList<IDAOIdentityUserClaim> daoIdentityUserClaimCollection = _iIdentityUserClaimRepository.SelectAllByCriteria(lstDataCriteria, lstDataOrder, dataSkip, dataTake);
				
				foreach(IDAOIdentityUserClaim resdaoIdentityUserClaim in daoIdentityUserClaimCollection)
					boIdentityUserClaimCollection.Add((T)(object)new BOIdentityUserClaim(resdaoIdentityUserClaim));
				
				return boIdentityUserClaimCollection;
			}
			catch
			{
				throw;
			}
		}
		
		
		///<Summary>
		///CollectionCount
		///This method returns the collection count of BOIdentityUserClaim objects, filtered by optional criteria
		///</Summary>
		///<returns>
		///Int32
		///</returns>
		///<parameters>
		///ICriteria icriteria
		///</parameters>
		public virtual Int32 CollectionCount(object o)
		{
			try
			{
				ICriteria icriteria = (ICriteria)o;
				List<IBOIdentityUserClaim> boIdentityUserClaimCollection = new List<IBOIdentityUserClaim>();
				IList<IDataCriterion> lstDataCriteria = (icriteria == null) ? null : icriteria.ListDataCriteria();
				Int32 objCount = _iIdentityUserClaimRepository.SelectAllByCriteriaCount(lstDataCriteria);
				
				return objCount;
			}
			catch
			{
				throw;
			}
		}
		
		#endregion

		#region member properties
		
		[InjectionPoint]
		public virtual IIdentityUserClaimRepository IdentityUserClaimRepository
		{
			set
			{
				_iIdentityUserClaimRepository = value;
			}
		}
		
		public virtual Int32? Id
		{
			get
			{
				 return _id;
			}
			set
			{
				_id = value;
				_isDirty = true;
			}
		}
		
		public virtual Int32? UserId
		{
			get
			{
				 return _userId;
			}
			set
			{
				_userId = value;
				_isDirty = true;
			}
		}
		
		public virtual string ClaimType
		{
			get
			{
				 return _claimType;
			}
			set
			{
				_claimType = value;
				_isDirty = true;
			}
		}
		
		public virtual string ClaimValue
		{
			get
			{
				 return _claimValue;
			}
			set
			{
				_claimValue = value;
				_isDirty = true;
			}
		}
		
		public virtual object Repository
		{
			get {	return (object) _iIdentityUserClaimRepository;	}
			set {	IdentityUserClaimRepository = (IIdentityUserClaimRepository)value;	}
		}
		
		public virtual bool IsDirty
		{
			get
			{
				 return _isDirty;
			}
			set
			{
				_isDirty = value;
			}
		}
		#endregion
	}
}
