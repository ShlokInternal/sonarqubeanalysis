/*************************************************************
** Class generated by CodeTrigger, Version 6.1.0.7
** This class was generated on 09-08-2018 PM 02:54:24
** Changes to this file may cause incorrect behaviour and will be lost if the code is regenerated
**************************************************************/
using System;
using System.Collections.Generic;
using CGSampleMVCWebAPI.Data.Interfaces;
using CGSampleMVCWebAPI.Business.Repository.Interfaces;

namespace CGSampleMVCWebAPI.Business.Interfaces
{
	///<Summary>
	///Interface definition
	///This is the interface definition for the class BOStudents.
	///</Summary>
	public partial interface IBOStudents 
	{
		#region class methods
		
		///<Summary>
		///SaveNew
		///This method persists a new Students record to the store
		///</Summary>
		///<returns>
		///void
		///</returns>
		///<parameters>
		///
		///</parameters>
		void SaveNew();
		
		///<Summary>
		///Update
		///This method updates one Students record in the store
		///</Summary>
		///<returns>
		///void
		///</returns>
		///<parameters>
		///BOStudents
		///</parameters>
		void Update();
		
		///<Summary>
		///Delete
		///This method deletes one Students record from the store
		///</Summary>
		///<returns>
		///void
		///</returns>
		///<parameters>
		///
		///</parameters>
		void Delete();
		
		#endregion

		#region member properties
		
		IStudentsRepository StudentsRepository	{	set;}
		
		Int32? Id	{	get;	set;}
		
		string FullName	{	get;	set;}
		
		Int32? Age	{	get;	set;}
		
		Int32? ClassID	{	get;	set;}
		
		Int32? StatusID	{	get;	set;}
		
		bool IsDirty	{	get;	set;}
		
		#endregion
	}
}
