/*************************************************************
** Class generated by CodeTrigger, Version 6.1.0.7
** This class was generated on 09-08-2018 PM 02:54:24
** Changes to this file may cause incorrect behaviour and will be lost if the code is regenerated
**************************************************************/
using System;
using System.Collections.Generic;
using CGSampleMVCWebAPI.Data.Interfaces;
using CGSampleMVCWebAPI.Business.Repository.Interfaces;

namespace CGSampleMVCWebAPI.Business.Interfaces
{
	///<Summary>
	///Interface definition
	///This is the interface definition for the class BOStudentStatuses.
	///</Summary>
	public partial interface IBOStudentStatuses 
	{
		#region class methods
		
		///<Summary>
		///SaveNew
		///This method persists a new StudentStatuses record to the store
		///</Summary>
		///<returns>
		///void
		///</returns>
		///<parameters>
		///
		///</parameters>
		void SaveNew();
		
		///<Summary>
		///Update
		///This method updates one StudentStatuses record in the store
		///</Summary>
		///<returns>
		///void
		///</returns>
		///<parameters>
		///BOStudentStatuses
		///</parameters>
		void Update();
		
		///<Summary>
		///Delete
		///This method deletes one StudentStatuses record from the store
		///</Summary>
		///<returns>
		///void
		///</returns>
		///<parameters>
		///
		///</parameters>
		void Delete();
		
		///<Summary>
		///StudentsCollection
		///This method returns its collection of BOStudents objects
		///</Summary>
		///<returns>
		///IList[IBOStudents]
		///</returns>
		///<parameters>
		///BOStudentStatuses
		///</parameters>
		IList<IBOStudents> StudentsCollection();
		
		///<Summary>
		///LoadStudentsCollection
		///This method loads the internal collection of BOStudents objects from storage. 
		///Call this if you need to ensure the collection is up-to-date (concurrency)
		///</Summary>
		///<returns>
		///void
		///</returns>
		///<parameters>
		///
		///</parameters>
		void LoadStudentsCollection();
		
		///<Summary>
		///AddStudents
		///This method persists a BOStudents object to the database collection
		///</Summary>
		///<returns>
		///void
		///</returns>
		///<parameters>
		///BOStudents
		///</parameters>
		void AddStudents(IBOStudents boStudents);
		
		///<Summary>
		///DeleteAllStudents
		///This method deletes all BOStudents objects from its collection
		///</Summary>
		///<returns>
		///void
		///</returns>
		///<parameters>
		///
		///</parameters>
		void DeleteAllStudents();
		
		#endregion

		#region member properties
		
		IStudentStatusesRepository StudentStatusesRepository	{	set;}
		
		IStudentsRepository StudentsRepository	{	set;}
		
		Int32? Id	{	get;	set;}
		
		string Name	{	get;	set;}
		
		string Description	{	get;	set;}
		
		bool IsDirty	{	get;	set;}
		
		#endregion
	}
}
