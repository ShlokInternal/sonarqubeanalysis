/*************************************************************
** Class generated by CodeTrigger, Version 6.1.0.7
** This class was generated on 09-08-2018 PM 02:54:24
** Changes to this file may cause incorrect behaviour and will be lost if the code is regenerated
**************************************************************/
using System;
using System.Collections.Generic;
using CGSampleMVCWebAPI.Data;
using CGSampleMVCWebAPI.Data.Interfaces;
using CGSampleMVCWebAPI.Business.Repository.Interfaces;

namespace CGSampleMVCWebAPI.Business.Repository
{
	///<Summary>
	///Class definition
	///This is the definition of the StudentStatusesRepository.
	///</Summary>
	public partial class StudentStatusesRepository : IStudentStatusesRepository, IDisposable
	{
		#region member variables
		IzCGSampleMVCWebAPIConn_TxConnectionProvider _connectionProvider;
		bool _isDisposed = false;
		#endregion

		#region disposable interface support
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		protected virtual void Dispose(bool isDisposing)
		{
			if(!_isDisposed)
			{
				if(isDisposing)
				{
					if(_connectionProvider != null)
					{
						((IDisposable)_connectionProvider).Dispose();
						_connectionProvider = null;
					}
				}
			}
			_isDisposed = true;
		}
		#endregion

		#region methods
		public virtual IzCGSampleMVCWebAPIConn_BaseData BaseData(IDAOStudentStatuses daoStudentStatuses)
		{	return (IzCGSampleMVCWebAPIConn_BaseData)(DAOStudentStatuses)daoStudentStatuses;	}

		public virtual IDAOStudentStatuses New()
		{	return new DAOStudentStatuses();	}

		public virtual void Insert(IDAOStudentStatuses daoStudentStatuses)
		{	daoStudentStatuses.Insert();	}

		public virtual void Update(IDAOStudentStatuses daoStudentStatuses)
		{	daoStudentStatuses.Update();	}

		public virtual void Delete(IDAOStudentStatuses daoStudentStatuses)
		{	daoStudentStatuses.Delete();	}

		public virtual IList<IDAOStudentStatuses> SelectAll()
		{	return DAOStudentStatuses.SelectAll();	}

		public virtual Int32 SelectAllCount()
		{	return DAOStudentStatuses.SelectAllCount();	}

		public virtual IDictionary<string, IList<object>> SelectAllByCriteriaProjection(IList<IDataProjection> listProjection, IList<IDataCriterion> listCriterion, IList<IDataOrderBy> listOrder, IDataSkip dataSkip, IDataTake dataTake)
		{	return DAOStudentStatuses.SelectAllByCriteriaProjection(listProjection, listCriterion, listOrder, dataSkip, dataTake);	}

		public virtual IList<IDAOStudentStatuses> SelectAllByCriteria(IList<IDataCriterion> listCriterion, IList<IDataOrderBy> listOrder, IDataSkip dataSkip, IDataTake dataTake)
		{	return DAOStudentStatuses.SelectAllByCriteria(listCriterion, listOrder, dataSkip, dataTake);	}

		public virtual Int32 SelectAllByCriteriaCount(IList<IDataCriterion> listCriterion)
		{	return DAOStudentStatuses.SelectAllByCriteriaCount(listCriterion);	}

		public virtual IDAOStudentStatuses SelectOne(Int32? id)
		{	return DAOStudentStatuses.SelectOne(id);	}

		#endregion

		#region properties
		public virtual IzCGSampleMVCWebAPIConn_TxConnectionProvider ConnectionProvider
		{
			get { return _connectionProvider; }
			set { _connectionProvider = value; }
		}

		#endregion

	}
}
