/*************************************************************
** Class generated by CodeTrigger, Version 6.1.0.7
** This class was generated on 09-08-2018 PM 02:54:24
** Changes to this file may cause incorrect behaviour and will be lost if the code is regenerated
**************************************************************/
using System;
using System.Collections.Generic;
using CGSampleMVCWebAPI.Data.Interfaces;

namespace CGSampleMVCWebAPI.Business.Repository.Interfaces
{
	///<Summary>
	///Interface definition
	///This is the interface definition for the generic repository
	///</Summary>
	public partial interface IGenericRepository<T> where T : class 
	{
		T New();
		IList<T> SelectAll();
		Int32 SelectAllCount();
		IDictionary<string, IList<object>> SelectAllByCriteriaProjection(IList<IDataProjection> dataProjection, IList<IDataCriterion> listCriterion, IList<IDataOrderBy> listOrder, IDataSkip dataSkip, IDataTake dataTake);
		IList<T> SelectAllByCriteria(IList<IDataCriterion> listCriterion, IList<IDataOrderBy> listOrder, IDataSkip dataSkip, IDataTake dataTake);
		Int32 SelectAllByCriteriaCount(IList<IDataCriterion> listCriterion);
		IzCGSampleMVCWebAPIConn_BaseData BaseData(T tbase);
	}
}
