/*************************************************************
** Class generated by CodeTrigger, Version 6.1.0.7
** This class was generated on 09-08-2018 PM 02:30:18
** Changes to this file may cause incorrect behaviour and will be lost if the code is regenerated
**************************************************************/
using System;
using System.Collections.Generic;
using CGSampleMVCWebAPI.Data.Interfaces;

namespace CGSampleMVCWebAPI.Business.Repository.Interfaces
{
	///<Summary>
	///Interface definition
	///This is the interface definition for the IdentityRole repository
	///</Summary>
	public partial interface IIdentityRoleRepository : IGenericRepository<IDAOIdentityRole>, IRepositoryConnection
	{
		void Insert(IDAOIdentityRole daoIdentityRole);
		void Update(IDAOIdentityRole daoIdentityRole);
		void Delete(IDAOIdentityRole daoIdentityRole);
		IDAOIdentityRole SelectOne(Int32? id);
	}
}
