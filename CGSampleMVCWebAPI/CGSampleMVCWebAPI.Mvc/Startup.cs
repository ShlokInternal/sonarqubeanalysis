using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CGSampleMVCWebAPI.Mvc.Startup))]
namespace CGSampleMVCWebAPI.Mvc
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
